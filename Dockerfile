FROM openjdk:8-jdk-alpine
MAINTAINER Piotr Pażuchowski <piotr.pazuchowski@gmail.com>

ADD ./target/place-service.jar /app/
CMD ["java", "-Xmx200m", "-jar", "/app/place-service.jar"]

EXPOSE 8080