package mgr.travel4you.placeservice;

import mgr.travel4you.placeservice.domain.Place;
import mgr.travel4you.placeservice.dto.PlaceCreateDto;
import mgr.travel4you.placeservice.service.PlaceService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import javax.annotation.PostConstruct;

/**
 * Bootstrap for microservice.
 */
@SpringBootApplication
@EnableCaching
public class PlaceServiceApplication {
	@Autowired
	private PlaceService placeService;

	@Autowired
	private ModelMapper modelMapper;

	/**
	 * Initialize start data.
	 */
	@PostConstruct
	private void initialize() {
		PlaceCreateDto placeCreateDto = new PlaceCreateDto();
		placeCreateDto.setName("Stare Miasto w Warszawie");
		placeCreateDto.setShortDescription(
				"Stare Miasto w Warszawie – najstarszy ośrodek miejski Warszawy będący zwartym zespołem architektury zabytkowej"
		);

		placeCreateDto.setLongDescription(
				"Stare Miasto zostało założone na skarpie, nad brzegiem dopływu Wisły – " +
				"rzeczki Kamionki, obok zamku książąt mazowieckich wzniesionego kilka lat wcześniej w miejscu najstarszej części obecnego Zamku Królewskiego, " +
				"na północ od wsi Warszawa. Jego początki sięgają XIII wieku. Wtedy otoczone było wałem ziemnym, który pod koniec XIV wieku został zastąpiony murem obronnym. " +
				"Ciągnął się on od skarpy wiślanej na zachód, wzdłuż dzisiejszej ulicy Podwale do Piekarskiej, następnie obok kościoła św. Marcina aż do zamku. " +
				"Uważa się, że założycielem miasta był Bolesław II, książę mazowiecki i płocki. Z uwagi na korzystne położenie, na szlaku handlowym znad Morza Czarnego ku Bałtykowi, " +
				"osiedlili się w Starej Warszawie głównie bogaci kupcy. " +
				"Pozostałość warszawskiego odcinka owej drogi handlowej to dzisiejsza ulica Świętojańska, Rynku i Nowomiejska." +
				"W środku miasta znajdował się plac w kształcie prostokąta, przy którym zamieszkało 40 najbogatszych osadników. " +
				"Każdy miał działkę o szerokości 9 m i długości 35 m. Taki kształt parceli zapewniał pomieszczenie na niewielkim terenie znacznej liczby przylegających do siebie domów. " +
				"Kamieniczki były wąskie, jedno lub dwupiętrowe, ze stromymi schodami i dwuspadowymi dachami. " +
				"Początkowo były drewniane, ale po pożarach w latach 1374, 1384, 1478, przybywało murowanych, wznoszonych z cegły. " +
				"Miał na to wpływ wprowadzony w 1431 zakaz budowy domów drewnianych w obrębie murów miejskich"
		);

		placeCreateDto.setCityId(1);
		placeCreateDto.setAvgRate(4.5);
		placeCreateDto.setLatitude(52.247733);
		placeCreateDto.setLongitude(21.013608);
		placeCreateDto.setImageUrlAdditional("/static/rsz_1280px-Plac_Zamkowy_w_Warszawie_widziany_z_wieży_kościoła_św._Anny.jpg");
		placeCreateDto.setImageUrlAdditional2("/static/rsz_1280px-warsaw_by_canaletto.jpg");
		placeCreateDto.setImageUrlAdditional3("/static/rsz_1280px-pol_old_market_square.jpg");


		PlaceCreateDto placeCreateDto1 = new PlaceCreateDto();
		placeCreateDto1.setName("Kolumna Zygmunta III Wazy w Warszawie");
		placeCreateDto1.setShortDescription(
				"Kolumna Zygmunta III Wazy w Warszawie – pomnik króla Zygmunta III Wazy znajdujący się na placu Zamkowym w Warszawie"
		);
		placeCreateDto1.setLongDescription(
				"Wzniesiona w 1644 kolumna Zygmunta jest najstarszym świeckim pomnikiem w Warszawie. " +
				"Została wystawiona z fundacji jego syna Władysława IV Wazy, który chciał w ten sposób uczcić pamięć swego ojca. " +
				"Kolumna była pomnikiem cnót królewskich i obrazowała świecką i sakralną istotę władzy królewskiej. " +
				"Jej wzniesienie miało cel polityczny (gloryfikacja dynastii Wazów i wzmocnienie władzy króla), " +
				"dlatego też została umieszczona na placu przed Bramą Krakowską – w tamtym czasie najważniejszym węźle komunikacyjnym stolicy." +
				"Projekt wywołał konflikt Władysława IV z nuncjuszem Mariem Filonardim. " +
				"Największe protesty nuncjusza wzbudziło umieszczenie na kolumnie posągu króla (osoby świeckiej). " +
				"Zgodnie z tradycją tego rodzaju wywyższenie było bowiem dotąd zarezerwowane dla Matki Boskiej, Chrystusa i świętych. " +
				"Koncepcję kolumny opracowali Augustyn Locci i Constantino Tencalla. " +
				"Brązowy posąg króla wykonał specjalnie sprowadzony z Bolonii Clemente Molli, a odlew z brązu oraz cztery tablice inskrypcyjne " +
				"– królewski giser Daniel Tym. Trzon kolumny wykonano z jednego bloku marmuru chęcińskiego pochodzącego z kamieniołomu w Czerwonej Górze. " +
				"Materiał odznaczał się strukturą zlepieńcową, z charakterystycznym wzorem „salcesonowym”. Z czasem ten gatunek zaczęto nazywać „Zygmuntówką”. " +
				"Kolumnę przetransportowano do Warszawy Wisłą. Z przystani została przetoczona, prawdopodobnie ul. Mostową, " +
				"i dalej wzdłuż murów obronnych na plac, w ciągu jednego dnia przez ok. 300 osób. Wspierało ich 200 żołnierzy z gwardii królewskiej."
		);
		placeCreateDto1.setCityId(1);
		placeCreateDto1.setAvgRate(4.0);
		placeCreateDto1.setLatitude(52.247253);
		placeCreateDto1.setLongitude(21.013378);
		placeCreateDto1.setImageUrlAdditional("/static/rsz_800px-pol_warsaw_zygmuntpct60s_column_2008_pct281pct29.jpg");
		placeCreateDto1.setImageUrlAdditional3("/static/rsz_1280px-warszawapct2c_stare_miastopct2c_stare_trzony_kolumny_zygmunta.jpg");
		placeCreateDto1.setImageUrlAdditional3("/static/rsz_krakow_gate_warsaw.jpg");

		PlaceCreateDto placeCreateDto2 = new PlaceCreateDto();
		placeCreateDto2.setName("Łazienki Królewskie w Warszawie");
		placeCreateDto2.setShortDescription("Łazienki Królewskie – zespół pałacowo-ogrodowy w Warszawie założony w XVIII wieku przez Stanisława Augusta Poniatowskiego. \n\n Nazwa pochodzi od barokowego pawilonu Łaźni, wzniesionego w latach 80. XVII wieku przez Stanisława Herakliusza Lubomirskiego i przebudowanego przez Stanisława Augusta Poniatowskiego.");
		placeCreateDto2.setLongDescription(
				"W średniowieczu w miejscu Łazienek znajdował się zwierzyniec służący jako miejsce polowań książąt mazowieckich[2].\n" +
				"W latach 80. XVII wieku ówczesny właściciel tego terenu, marszałek wielki koronny Stanisław Herakliusz Lubomirski, zbudował tam dwa pawilony: Ermitaż oraz bogato dekorowany pawilon Łaźni. Obydwa zostały zaprojektowane przez Tylmana z Gameren[2].\n" +
				"We wrześniu 1764 Ujazdów wraz ze zwierzyńcem został kupiony od Elżbiety (wdowy po Teodorze Lubomirskim) i jej syna Kacpra, przez Stanisława Augusta Poniatowskiego (w tamtym czasie jeszcze stolnika litewskiego)[3]. Przygotowując się do roli króla, szukał on poza miastem miejsca na swoją prywatną rezydencję[3]. Na terenie dawnego zwierzyńca przeprowadzono prace melioracyjne i wykarczowano stare olchy[4]. Jego przekształcanie w letnią rezydencję królewską – krajobrazowy park angielski ozdobiony pawilonami – rozpoczęto w 1774, po zarzuceniu rozpoczętego projektu przebudowy Zamku Ujazdowskiego[5]. Pierwszym budynkiem zbudowanym przez króla w Łazienkach był Biały Domek[2]. W 1784 w miejscu niewielkiej sadzawki powstałej w czasach Lubomirskego wykopano dwa duże stawy, na północ i południe od przebudowanego z pawilonu Łaźni pałacu Na Wyspie. Staw południowy otrzymał fantazyjną linię brzegową z romantyczną kępą, na której wzniesiono scenę Amfiteatru[6]. Był on wzorowany na teatrze w Herkulanum[7]."
		);

		placeCreateDto2.setCityId(1);
		placeCreateDto2.setAvgRate(5.0);
		placeCreateDto2.setLatitude(52.215153);
		placeCreateDto2.setLongitude(21.034999);
		placeCreateDto2.setImageUrlAdditional("/static/rsz_1280px-palaceontheisle.jpg");
		placeCreateDto2.setImageUrlAdditional2("/static/rsz_1280px-lazienki_krolewskie.jpg");
		placeCreateDto2.setImageUrlAdditional3("/static/rsz_1280px-jm3_dsc0717.jpg");

		PlaceCreateDto placeCreateDto3 = new PlaceCreateDto();
		placeCreateDto3.setName("Sukiennice we Wrocławiu");
		placeCreateDto3.setShortDescription("Sukiennice we Wrocławiu, zwane też Domem Kupców[1] – nieistniejący już zespół średniowiecznych budynków w centralnej części wrocławskiego Rynku, w miejscu dzisiejszej ulicy Sukiennice. Stanowiły jeden z trzech historycznych wrocławskich domów kupieckich[2].");
		placeCreateDto3.setLongDescription("Wzmiankowany po raz pierwszy w XIII-wiecznej korespondencji mieszczan z Magdeburga do władcy Wrocławia (prawdopodobnie do Henryka Brodatego)[3]; pojawia się także później w dokumentach z 11 marca 1242, 16 grudnia 1261, 31 stycznia 1272 (prawdopodobnie XIV-wieczny falsyfikat). W 1305 Bolesław III Rozrzutny wydał przywilej zakazujący krojenia sukna i handlu nim gdziekolwiek w mieście poza komorami sukienników.\n\n Na podstawie dokumentacji sporządzonej podczas rozbiórki sukiennic w 1821 oraz badań archeologicznych prowadzonych pod koniec XX wieku ustalono, że komory sukienników mieściły się w czterech podłużnych budynkach ustawionych wzdłuż szerokiego dziedzińca biegnącego w przybliżeniu w kierunku wschód-zachód (jego przebieg pokrywa się z linią współczesnej ulicy Sukiennice). Dziedziniec ten przecinało prostopadłe wąskie przejście[4], rozdzielające także budynki sukiennic na dwie pary, oraz przecinające cały blok śródrynkowy na dwie części. Budynki sukiennic nie powstały jednocześnie, ostatnim był zbudowany pod koniec XIII wieku blok północno-zachodni, który wzniesiono w wątku gotyckim, pozostałe zaś w wątku wendyjskim. Budynki po wschodniej stronie Przejścia Poprzecznego miały 61,6 metra, a po zachodniej – 32 metry długości. Zawierały odpowiednio 14 i 7 komór o wymiarach wewnętrznych około 6,2×3,7 metra. Komory nie posiadały okien, a jedynie otwór drzwiowy, zamykany w celu zabezpieczenia przechowywanego towaru. Wejścia do komór wiodły ze wspólnego środkowego dziedzińca przez niewielkie otwarte przedsionki, w których prawdopodobnie odbywał się handel. W XIV wieku sukiennice podpiwniczono, być może dopiero wówczas wzniesiono też piętro nad komorami. Całe sukiennice miały około 96 metrów długości i 30 metrów szerokości.");
		placeCreateDto3.setCityId(2);
		placeCreateDto3.setAvgRate(4.6);
		placeCreateDto3.setLatitude(51.109969);
		placeCreateDto3.setLongitude(17.031847);
		placeCreateDto3.setImageUrlAdditional("/static/180px-New_Town_Hall_Photo_Sukiennice_Street_Gateway_East_View_Wrocław_Poland_2008-11-14.jpg");
		placeCreateDto3.setImageUrlAdditional2("/static/rsz_1024px-new_town_hall_photo_sukiennice_street_gateway_wrocław_poland_2008-11-14.jpg");

		PlaceCreateDto placeCreateDto4 = new PlaceCreateDto();
		placeCreateDto4.setName("Gubałówka");
		placeCreateDto4.setShortDescription("Gubałówka – podłużne wzniesienie na Pogórzu Gubałowskim (Paśmie Gubałowskim).");
		placeCreateDto4.setLongDescription("Ma wysokość od 1120 m na wschodnim końcu do 1129 m na zachodnim końcu. Wznosi się w północno-zachodniej części Zakopanego. Po wschodniej stronie Gubałówki znajduje się miejscowość Ząb, a po zachodniej Gubałówka sąsiaduje z Butorowym Wierchem. U podnóża północnych stoków Gubałówki w dolinie potoku Bystrego położone jest Nowe Bystre[1]. Nazwa Gubałówka pochodzi od znajdującej się na niej polany Gubałówka, tej zaś od nazwiska Gubała[2]. \n \n Na Gubałówkę prowadzi kolej linowo-terenowa z Zakopanego wybudowana w 1938 roku, długości 1298 m oraz różnicy wysokości ok. 300 m., zmodernizowana w 2001 roku. Góra jest popularna wśród turystów odwiedzających Zakopane. Z jej grzbietu roztacza się panorama całych polskich Tatr i Zakopanego oraz Podhala, Pienin, Gorców i Beskidu Żywieckiego wraz z ich słowacką częścią. Z Gubałówki prowadzi spacerowy Szlak Papieski na Butorowy Wierch 1160 m, z którego można zjechać kolejką krzesełkową do Kościeliska. Na skutek sporów pomiędzy właścicielami gruntów na zboczu Gubałówki a Polskimi Kolejami Linowymi zjazd narciarski do dolnej stacji kolejki jest aktualnie niemożliwy.");
		placeCreateDto4.setCityId(3);
		placeCreateDto4.setAvgRate(5.0);
		placeCreateDto4.setLatitude(49.307427);
		placeCreateDto4.setLongitude(19.937017);
		placeCreateDto4.setImageUrlAdditional("/static/rsz_1280px-pasmo_gubałowskie_t57-2.jpg");
		placeCreateDto4.setImageUrlAdditional2("/static/rsz_1280px-gubałówka_02.jpg");

		PlaceCreateDto placeCreateDto5 = new PlaceCreateDto();
		placeCreateDto5.setName("Morskie Oko");
		placeCreateDto5.setShortDescription("Morskie Oko - największe jezioro w Tatrach, położone w Dolinie Rybiego Potoku u stóp Mięguszowieckich Szczytów, na wysokości 1395 m n.p.m.");
		placeCreateDto5.setCityId(3);
		placeCreateDto5.setImageUrlAdditional("/static/rsz_morskie_oko_o_swicie.jpg");
		placeCreateDto5.setImageUrlAdditional2("/static/rsz_1280px-morskie_oko_ze_szlaku_przez_Świstówkę.jpg");
		placeCreateDto5.setImageUrlAdditional3("/static/rsz_1280px-panorama-morskiego-oka.jpg");


		placeService.createPlace(modelMapper.map(placeCreateDto, Place.class));

		placeService.createPlace(modelMapper.map(placeCreateDto1, Place.class));
		placeService.createPlace(modelMapper.map(placeCreateDto2, Place.class));
		placeService.createPlace(modelMapper.map(placeCreateDto3, Place.class));
		placeService.createPlace(modelMapper.map(placeCreateDto4, Place.class));
		placeService.createPlace(modelMapper.map(placeCreateDto5, Place.class));
	}

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(PlaceServiceApplication.class, args);
	}
}
