package mgr.travel4you.placeservice.repository;

import mgr.travel4you.placeservice.domain.Place;
import mgr.travel4you.placeservice.dto.PlaceReadDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Place repository.
 */
@Repository
public interface PlaceRepository extends CrudRepository<Place, Long> {
    Iterable<Place> findAllByCityId(Integer cityId);
}
