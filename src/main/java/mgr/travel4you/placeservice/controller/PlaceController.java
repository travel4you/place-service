package mgr.travel4you.placeservice.controller;

import mgr.travel4you.placeservice.domain.Place;
import mgr.travel4you.placeservice.dto.PlaceReadDto;
import mgr.travel4you.placeservice.dto.PlaceUpdateDto;
import mgr.travel4you.placeservice.exception.PlaceNotFoundException;
import mgr.travel4you.placeservice.service.PlaceService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * The type Place controller.
 */
@RestController
@CrossOrigin
@RequestMapping(path = "/api/v1")
public class PlaceController {

    @Autowired
    private PlaceService placeService;

    @Autowired
    private ModelMapper modelMapper;

    /**
     * Read place list list.
     *
     * @return the list
     */
    @GetMapping(path = "/place")
    public List<PlaceReadDto> readPlaceList(
            @RequestParam(value = "cityId", required = false) Integer cityId
    ) {
        Iterable<Place> result;

        if (cityId != null) {
            result = placeService.readPlaceListByCityId(cityId);
        } else {
            result = placeService.readPlaceList();
        }


        return StreamSupport
                .stream(result.spliterator(), true)
                .map(place -> modelMapper.map(place, PlaceReadDto.class))
                .collect(Collectors.toList());
    }

    /**
     * Read place response entity.
     *
     * @param id the id
     * @return the response entity
     * @throws PlaceNotFoundException the place not found exception
     */
    @GetMapping(path = "/place/{id}")
    public ResponseEntity<PlaceReadDto> readPlace(@PathVariable Long id) throws PlaceNotFoundException {
        return placeService
                .readPlaceById(id)
                .map(place -> modelMapper.map(place, PlaceReadDto.class))
                .map(place -> ResponseEntity.ok().body(place))
                .orElseThrow(PlaceNotFoundException::new);
    }

    /**
     * Update place response entity.
     *
     * @param id   the id
     * @param body the body
     * @return the response entity
     * @throws PlaceNotFoundException the place not found exception
     */
    @PutMapping(path = "/place/{id}")
    public ResponseEntity<Void> updatePlace(@PathVariable Long id, @RequestBody @Valid PlaceUpdateDto body) throws PlaceNotFoundException {
        Place fetchedPlace = placeService
                .readPlaceById(id)
                .orElseThrow(PlaceNotFoundException::new);

        Place placeMappedByDto = modelMapper.map(body, Place.class);

        placeMappedByDto.setId(id);
        placeMappedByDto.setCreatedAt(fetchedPlace.getCreatedAt());

        placeService.updatePlace(placeMappedByDto);

        return ResponseEntity.noContent().build();

    }

    /**
     * Delete place response entity.
     *
     * @param id the id
     * @return the response entity
     * @throws PlaceNotFoundException the place not found exception
     */
    @DeleteMapping(path = "/place/{id}")
    public ResponseEntity<Void> deletePlace(@PathVariable Long id) throws PlaceNotFoundException  {
        Place place = placeService
                .readPlaceById(id)
                .orElseThrow(PlaceNotFoundException::new);

        placeService.deletePlace(place);

        return ResponseEntity.noContent().build();
    }
}
