package mgr.travel4you.placeservice.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * The Place Dto dedicated for create action.
 */
@Getter
@Setter
@NoArgsConstructor
public class PlaceCreateDto {
    private Integer cityId;

    private Double latitude;
    private Double longitude;

    private Double avgRate;

    private String name;

    private String shortDescription;

    private String longDescription;

    private String imageUrlAdditional;
    private String imageUrlAdditional2;
    private String imageUrlAdditional3;

    private LocalDateTime createdAt = LocalDateTime.now();
    private LocalDateTime editedAt = LocalDateTime.now();
}
