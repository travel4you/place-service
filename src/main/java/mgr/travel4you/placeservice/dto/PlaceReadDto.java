package mgr.travel4you.placeservice.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 * The Place Dto dedicated for read action.
 */
@Getter
@Setter
@NoArgsConstructor
public class PlaceReadDto {
    @Id
    private Long id;

    private Integer cityId;

    private Double latitude;
    private Double longitude;

    private Double avgRate;

    private String name;

    private String shortDescription;

    private String longDescription;

    private String imageUrlAdditional;
    private String imageUrlAdditional2;
    private String imageUrlAdditional3;

    private LocalDateTime createdAt;
    private LocalDateTime editedAt;
}
