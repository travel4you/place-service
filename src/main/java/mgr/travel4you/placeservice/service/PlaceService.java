package mgr.travel4you.placeservice.service;

import mgr.travel4you.placeservice.domain.Place;
import mgr.travel4you.placeservice.repository.PlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * The type Place service.
 */
@Service
public class PlaceService {
    @Autowired
    private PlaceRepository repository;

    /**
     * Read place list iterable.
     *
     * @return the iterable
     */
    @Cacheable(value = "places")
    public Iterable<Place> readPlaceList() {
        return repository.findAll();
    }

    /**
     * Read place list by city id.
     *
     * @return the iterable
     */
    @Cacheable(value = "places-for-city", key="#cityId")
    public Iterable<Place> readPlaceListByCityId(Integer cityId) {
        return repository.findAllByCityId(cityId);
    }

    /**
     * Read place by id optional.
     *
     * @param id the id
     * @return the optional
     */
    @Cacheable(value = "places", key="#id")
    public Optional<Place> readPlaceById(Long id) {
        return repository.findById(id);
    }

    /**
     * Create place place.
     *
     * @param place the place
     * @return the place
     */
    @CacheEvict(value={"places", "places-for-city"})
    public Place createPlace(Place place) {
        return repository.save(place);
    }

    /**
     * Update place place.
     *
     * @param place the place
     * @return the place
     */
    @CachePut(value="places", key="#place.getId()")
    public Place updatePlace(Place place) {
        return repository.save(place);
    }

    /**
     * Delete place.
     *
     * @param place the place
     */
    @CacheEvict(value="places", key="#place.getId()")
    public void deletePlace(Place place) {
        repository.delete(place);
    }
}
