package mgr.travel4you.placeservice.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * The Place Entity.
 */
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class Place implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer cityId;

    private Double latitude;
    private Double longitude;

    private Double avgRate;

    private String name;

    @Column(length = 1024)
    private String shortDescription;

    @Column(columnDefinition = "TEXT")
    private String longDescription;

    private String imageUrlAdditional;
    private String imageUrlAdditional2;
    private String imageUrlAdditional3;

    private LocalDateTime createdAt;
    private LocalDateTime editedAt;
}