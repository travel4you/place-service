package mgr.travel4you.placeservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Requested place didn't find in Place repository.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Sorry, requested place doesn't exists!")
public class PlaceNotFoundException extends Exception {
    private static final long serialVersionUID = 100L;
}